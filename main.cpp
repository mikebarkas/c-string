#include <cstdio>
using namespace std;

/*
 * String fundamentals.
 */

int main(int argc, char **argv) {
  
  // A sting is an array of characters.
  char s[] = { 's', 't', 'r', 'i', 'n', 'g', 0};
  printf("String is: %s\n", s);

  return 0;
}

